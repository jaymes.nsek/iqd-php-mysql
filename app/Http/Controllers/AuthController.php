<?php

namespace App\Http\Controllers;

use App\model\AppErrMsg;
use App\model\ConnectionHelper;
use App\model\StatusCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\Rules\Password;
use mysqli as mysqli;
use mysqli_sql_exception;
use Throwable;

class AuthController extends Controller
{
    public function create(Request $req): JsonResponse
    {
        // Validate Request params
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => ['required', Password::min(8)->symbols()->numbers()->mixedCase()]
        ]);

        if ($validator->fails())
            // If validation fails return error object to frontend (which contains input
            // param, along with reason for failure, styled, for example, as:
            // {"email":["The email must be a valid email address."], ...}
            return $this->jsonRes($validator->errors());

        // Connect
        $connHelper = ConnectionHelper::getInstance();

        if (!($conn = $connHelper->connect()))
            return $this->jsonRes($connHelper::getErrMsg(), $connHelper::getStatusCode());

        // Targeted at SQL Injection (Add protection in addition to Validation)
        $email = $conn->real_escape_string($req->input('email'));
        $pw = $conn->real_escape_string($req->input('password'));

        // Targeted at HTML Injection (Add protection in addition to Validation)
        $email = htmlspecialchars($email, ENT_QUOTES);

        // ====================== TODO: Hash the password ======================
        $hash = $pw;

        // Prepare (returns Object/bool)
        if (!($stmt = $conn->prepare('INSERT INTO Users VALUES(?, ?, ?)')))
            return $this->jsonRes(AppErrMsg::PREPARED_STMT_ERR);

        // Bind and Execute (returns bool)
        $id = 0;
        if (!$stmt->bind_param('iss', $id, $email, $hash))
            return $this->jsonRes(AppErrMsg::PREPARED_STMT_BIND_ERR);

        // Attempt to add user, return if constraint Error occurs
        try {
            if (!$stmt->execute())
                return $this->jsonRes(AppErrMsg::PREPARED_STMT_EXECUTION_ERR);
        } catch (\mysqli_sql_exception $e) {
            if ($e->getCode() == 1062)// Code: 1062 = mysqli_sql_exception: Duplicate entry
                return $this->jsonRes(AppErrMsg::DUPLICATE_ENTRY);
            return $this->jsonRes(AppErrMsg::PREPARED_STMT_EXECUTION_ERR);
        }

        // On success
        $conn->close();
        return $this->jsonRes(null, StatusCode::CREATED, ['Return JWT']);
    }

    /**
     * Helper method, returns a  {@link JsonResponse} that adheres to project-wide format.
     *
     * @param MessageBag|string|null $errMsg error msg to indicate a specific cause; if none, i.e.
     *        then null should be returned. If a {@link Throwable} object, it is jsonified.
     * @param array|int|null $data the requested or confirmatory data, e.g. rows affected.
     * @param StatusCode $status Accepted-project HTTP Status codes
     * @return JsonResponse
     */
    private function jsonRes(MessageBag|AppErrMsg|string|null $errMsg,
                             StatusCode                       $status = StatusCode::BAD_REQUEST,
                             array|int|null                   $data = null): JsonResponse
    {
        return \response()->json(
            ["data" => $data, "errors" => $errMsg],
            $status->value
        );
    }
}
