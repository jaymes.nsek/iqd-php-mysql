<?php

namespace App\model;

/**
 * The helper class centralises all expected error messages in the app, achieving to main goals:
 * 1. For functions or classes that the same error type, customising the error message in the
 *    ResponseJSON allows the exact returning point in code to be validated through the
 *    expected-accompanying message.
 * 2. Messages are aligned between frontend, backend and testcases.
 */
enum AppErrMsg: string
{
    case DB_CONNECTION_FAILED = 'DB Connection failed.';
    case SCHEMA_INIT_FAILED = 'Initialising MySQL tables error.';
    case PREPARED_STMT_ERR = 'PreparedStatement creation error';
    case PREPARED_STMT_BIND_ERR= 'Statement Bind error.';
    case DUPLICATE_ENTRY = 'Unique Constraint breached.';
    case PREPARED_STMT_EXECUTION_ERR = 'Error whilst executing PreparedStatement.';
}
