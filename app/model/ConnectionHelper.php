<?php

namespace App\model;

use Illuminate\Support\Facades\Log;
use mysqli;
use mysqli_sql_exception;

/**
 * Sole purpose of this class is to allow dynamic environment variable changes to allow
 * testing of SQL connection failures in Feature Testing.
 *
 * @note It was found that although using {@link putenv()} in TestCase changed the variables,
 * as verified by {@link getenv()}, this did not have an impact on the test; and init values
 * still persisted for the connection; hence, this approach.
 */
final class ConnectionHelper
{
    static private ?ConnectionHelper $instance = null;

    static private ?mysqli $conn = null;

    static private string $hn;
    static private string $un;
    static private string $pw;
    static private string $db;

    static private AppErrMsg|null $errMsg;
    static private StatusCode $statusCode;

    // Create tables if they do not already exist
    static private string $createSchema = "CREATE TABLE IF NOT EXISTS Users (
                                        id INT NOT NULL AUTO_INCREMENT,
                                        email VARCHAR(64) NOT NULL UNIQUE,
                                        hash VARCHAR(512) NOT NULL,
                                        PRIMARY KEY (id)
                                     )";

    private function __construct()
    {
    }

    static public function getInstance(): self
    {
        if (self::$instance == null) {
            self::initEnvVar();
            self::$instance = new self();
        }
        return self::$instance;
    }

    //region Environment Variables methods

    /**
     * Sets default {@link mysqli} connection params to environment variables values;
     * to be used as default params in {@link makeConnection}.
     */
    static private function initEnvVar()
    {
        self::$hn = $_ENV['DB_HOST'];
        self::$un = $_ENV['DB_USERNAME'];
        self::$pw = $_ENV['DB_PASSWORD'];
        self::$db = $_ENV['DB_DATABASE'];
    }

    /**
     * Return an object containing MySQLi connection params of host (Name),
     * user (Name), password and database (Name).
     *
     * These values are used for the mysqli connection when {@link makeConnection} is
     * called directly or via {@link connect).
     *
     * @note intended for use in Testing!
     */
    public function getEnvVar(): object
    {
        return (object)["host" => self::$hn, "user" => self::$un,
            "password" => self::$pw, "database" => self::$db];
    }

    /**
     * Attempt to connect to the DB with the requested connection params; if any of the arg is
     * null, then are nonnull the default .env variables is substituted for all.
     *
     * @return mysqli|null a mysqli object if successful, else null.
     */
    static private function makeConnection(string|null $hn, string|null $un,
                                           string|null $pw, string|null $db): ?mysqli
    {
        // Connect with default .env variables if any arg is null
        if ($hn == null || $un == null || $pw == null || $db == null) {
            $hn = self::$hn;
            $un = self::$un;
            $pw = self::$pw;
            $db = self::$db;
        }

        try {
            self::$conn = new mysqli($hn, $un, $pw, $db);
            // throw exception so that it is also handle with the same catch pattern
            if (self::$conn->connect_error)
                throw new mysqli_sql_exception;

            // Init schema as required
            self::$conn->query(self::getSqlInit());
            self::$errMsg = null;
            self::$statusCode = StatusCode::CREATED;

            Log::channel('stderr')->info(" ++++++++++ MySQLi conn success");
            return self::$conn;
        } catch (mysqli_sql_exception) {
            self::$errMsg = AppErrMsg::DB_CONNECTION_FAILED;
            self::$statusCode = StatusCode::SERVER_ERR;
            return null;
        }
    }

    /**
     * Attempt to connect to the MySQL database; using the given params, if all are nonnull, or
     * the default .env variables for all.
     *
     * Additionally:
     *
     * In production mode - If the connection is alive, return it; if dead, attempt to
     * reconnect with set DB connection params first, before returning.
     *
     * When in debug mode - force connection with applicable connection variables;
     * regardless of if the connection is alive.
     *
     * Exposes {@link makeConnection} as an instance method.
     *
     * @note Args are intended primarily for use in Testing only! And they are not saved so
     * that deviation from environment variables must always be explicitly stated.
     * @return mysqli|null
     */
    final public function connect(string|null $hn = null, string|null $un = null,
                                  string|null $pw = null, string|null $db = null): ?mysqli
    {
        if ($_ENV['APP_DEBUG']) {
            Log::channel('stderr')->info(" --------- NEW CONNECTION PATH");
            return self::makeConnection($hn, $un, $pw, $db);
        } else {
            Log::channel('stderr')->info(" --------- OLD CONN PATH");
            return (!self::$conn || !self::$conn->ping()) ?
                self::makeConnection($hn, $un, $pw, $db) : self::$conn;
        }
    }

    //endregion

    static private function getSqlInit(): string
    {
        return self::$createSchema;
    }

    /**
     * @return AppErrMsg the last generated error message when connection failed,
     * signified by return of null after {@link connect} is called.
     */
    public static function getErrMsg(): AppErrMsg
    {
        return self::$errMsg;
    }

    /**
     * @return StatusCode the last generated error status code corresponding to
     * the {@link getErrMsg} call.
     */
    public static function getStatusCode(): StatusCode
    {
        return self::$statusCode;
    }
}

// Closing tag is IMPORTANT here, to ensure these values are only backend interpretable
?>
