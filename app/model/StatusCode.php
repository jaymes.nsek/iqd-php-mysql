<?php

namespace App\model;

/**
 * Documents App-wide accepted Response status codes.
 */
enum StatusCode: int
{
    // Successful
    case OK = 200; // OK
    case CREATED = 201; // CREATED - Should be used for POST req only

    case BAD_REQUEST = 400;

    case SERVER_ERR = 500; // Internal Server Error
}
