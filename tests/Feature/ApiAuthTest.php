<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ApiAuthTest extends TestCase
{
    use RefreshDatabase;

    //region ValidationError Tests

    /**
     * api/auth/create request without email and password, returns statusCode 400.
     */
    public function testCreateAccount_withNoEmailAndOrPasswordFails(): void
    {
        $response = $this->postJson('/api/auth/create');

        // Assert code and structure; Reasons array, accompanying error keys - to indicate
        // cause of failure - need not be tested, as this are prone to framework modifications!
        $response->assertStatus(400)
            ->assertJsonStructure([
                'data',
                'errors' => ['email', 'password']
            ]);
    }

    /**
     * api/auth/create request with invalid email, returns statusCode 400.
     *
     * @dataProvider validPasswordInvalidEmailsProvider
     * @note password used for the test should be valid!
     */
    public function testCreateAccount_withInvalidEmailFails(string $email, string $pw): void
    {
        $response = $this->postJson('/api/auth/create', ['email' => $email, 'password' => $pw]);

        // Assert code and structure;
        $response->assertStatus(400)
            ->assertJsonStructure([
                'data',
                'errors' => ['email']
            ]);
    }

    public function validPasswordInvalidEmailsProvider(): array
    {
        $validPassword = 'Ant125#u2';

        return [
            'Email without @ sign' => ['email_without_at_sign_gmail.com', $validPassword],
            'Email without second-level domain' => ['email_without_domain@.com', $validPassword],
            'Email without top-level domain' => ['email_without_domain@gmail.', $validPassword],
            'XSS slipped in with email' =>
                ['<button type="button" onclick="alert(\'XSS\')"> correct_email_here </button>',
                    $validPassword]
        ];
    }

    /**
     * api/auth/create request with invalid password, returns statusCode 400.
     *
     * @dataProvider invalidPasswordsValidEmailProvider
     */
    public function testCreateAccount_withInvalidPasswordFails(string $email, string $pw): void
    {
        $response = $this->post('/api/auth/create', ["email" => $email, "password" => $pw]);

        // Assert code and structure;
        $response->assertStatus(400)
            ->assertJsonStructure([
                'data',
                'errors' => ['password']
            ]);
    }

    public function invalidPasswordsValidEmailProvider(): array
    {
        $validEmail = 'joe_blogs@gmail.com';

        return [
            'Password < min=8' => [$validEmail, 'Ab1#'],
            'Password contains no Uppercase' => [$validEmail, 'abcde1234#'],
            'Password contains no Lowercase' => [$validEmail, 'ABCDE1234#'],
            'Password contains no Symbol' => [$validEmail, 'Abcde1234'],
            'Password contains no Number' => [$validEmail, 'Abcdefghi#']
        ];
    }

    /**
     * api/auth/create request with VALID email and password, returns statusCode 201,
     * and error is set null.
     *
     * @dataProvider validPasswordAndEmailProvider
     */
    public function testValidEmailAndPasswordSucceed(string $email, string $pw)
    {
        $response = $this->post('/api/auth/create', ["email" => $email, "password" => $pw]);
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data',
                'errors'
            ])->assertJson( ['errors' => null]);
    }

    public function validPasswordAndEmailProvider(): array
    {
        return [
            ['joe.blogs@gmail.com', 'Abcde1234/'],
            ['joe-blogs-one@yahoo.com', 'Abcde1234+*']
        ];
    }

    //endregion
}
