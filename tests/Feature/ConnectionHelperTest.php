<?php

namespace Tests\Feature;

use App\model\ConnectionHelper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ConnectionHelperTest extends TestCase
{

    /**
     * @test
     * Environment values are init
     */
    public function getInstanceInitsEnvVars(): void
    {
        $envVars = ConnectionHelper::getInstance()->getEnvVar();

        self::assertEquals($_ENV['DB_HOST'], $envVars->host);
        self::assertEquals($_ENV['DB_USERNAME'], $envVars->user);
        self::assertEquals($_ENV['DB_PASSWORD'], $envVars->password);
        self::assertEquals($_ENV['DB_DATABASE'], $envVars->database);
    }


}
